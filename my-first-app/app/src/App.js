import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {lists: []}
    this.input1 = React.createRef()
  }


  addShoppingList = async () => {
    let res = await axios.post('http://servernode:4444/shopping-list', {name:this.input1.current.value});
    // console.log(res.data);
    this.setState(state => ({
      lists: [...state.lists, {name:this.input1.current.value, id: res.data}]
    }))
  }

  async componentDidMount() {
    let res = await axios.get('http://servernode:4444/shopping-list');
    let data = res.data;
    console.log(data);
    this.setState({lists:data});       
  }

  deleteShoppingList = async (id) => {
    console.log(id);
    let res = await axios.delete('http://servernode:4444/shopping-list/'+id);
    // this.setState(state => ({
    //   id: ''
    // }));
    let res2 = await axios.get('http://servernode:4444/shopping-list');
    let data = res2.data;
    this.setState({lists:data});  

  }


  render() {
    return (
      <div className="App">

        <input  ref={this.input1} />
        <br/>
        <button className="peter-river-flat-button" onClick={this.addShoppingList}>Ajouter un objet</button>
        <table>
          <thead>
            <tr><th>Nom</th><th>Id</th><th></th></tr>
          </thead>
          <tbody>
            {this.state.lists.map(e => (
                <tr key={e.id}><td>{e.name}</td><td>{e.id}</td>
                <td><button className="pomegranate-flat-button" onClick={() => this.deleteShoppingList(e.id)}>Supprimer</button></td>
                </tr>
              ))}
          </tbody>
        </table>
        
      </div>
    );
  }
}

export default App;

